package ru.dolbak.game2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

import java.util.Random;

public class MyDraw extends View {
    double x = 0;
    double y = 0;
    int r = 35;
    int m = 20;
    int array[][];
    boolean initial = true;

    public MyDraw(Context context) {

        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);

        if (initial){
            Random rand = new Random();
            array = new int[canvas.getHeight() / (r*2 + m)]
                    [canvas.getWidth() / (r*2 + m)];
            for (int i = 0; i < array.length; i++){
                for (int j = 0; j < array[i].length; j++){
                    array[i][j] = rand.nextInt(2);
                }
            }
            initial = false;
        }

        for (int i = 0; i < array.length; i++){
            for (int j = 0; j < array[i].length; j++){
                if (array[i][j] == 0){
                    paint.setStyle(Paint.Style.STROKE);
                }
                else{
                    paint.setStyle(Paint.Style.FILL_AND_STROKE);
                }
                canvas.drawCircle(j * (r*2+m) + r + m, i * (r*2+m) + r + m, r, paint);
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int touch = event.getAction();
        if (touch == MotionEvent.ACTION_DOWN){
            x = event.getX();
            y = event.getY();
            int xi = (int) (x/(m+2*r));
            int yi = (int) (y/(m+2*r));
            if (array[yi][xi] == 1){
                array[yi][xi] = 0;
            }
            else{
                array[yi][xi] = 1;
            }
        }
        invalidate();
        return true;
    }
}

